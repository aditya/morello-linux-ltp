/*
 *
 *   Copyright (c) International Business Machines  Corp., 2009
 *   Copyright (c) 2014 Oracle and/or its affiliates. All Rights Reserved.
 *
 *   This program is free software;  you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY;  without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 *   the GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program;  if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include "test.h"
#include "tst_pid.h"
#include "old_safe_file_ops.h"
#include "tst_safe_macros.h"

#define PID_MAX_PATH "/proc/sys/kernel/pid_max"
/* Leave some available processes for the OS */
#define PIDS_RESERVE 50

pid_t tst_get_unused_pid_(void (*cleanup_fn) (void))
{
	pid_t pid;

	SAFE_FILE_SCANF(cleanup_fn, PID_MAX_PATH, "%d", &pid);

	return pid;
}

static int __read_pids_limit(const char *path, void (*cleanup_fn) (void))
{
	char max_pids_value[100];
	int max_pids;

	if (access(path, R_OK) != 0) {
		tst_resm(TINFO, "Cannot read session user limits from '%s'", path);
		return -1;
	}

	SAFE_FILE_SCANF(cleanup_fn, path, "%s", max_pids_value);
	if (strcmp(max_pids_value, "max")) {
		max_pids =  SAFE_STRTOL(max_pids_value, 0, INT_MAX);
		tst_resm(TINFO, "Found limit of processes %d (from %s)",
				max_pids, path);
	} else {
		max_pids = -1;
	}

	return max_pids;
}

/*
 * Take the path to the cgroup mount and to the current cgroup pid controller
 * and try to find the PID limit imposed by cgroup.
 * Go up the cgroup hierarchy if needed, otherwise use the kernel PID limit.
 */
static int read_session_pids_limit(const char *cgroup_mount,
				   const char *cgroup_path, void (*cleanup_fn) (void))
{
	int ret, cgroup_depth = 0, max_pids = -1;
	char path[PATH_MAX + 1], file_path[PATH_MAX + 1];
	const char *sub_path = cgroup_path;

	/* Find the number of groups we can go up. */
	do {
		cgroup_depth += 1;
		sub_path++;
		sub_path = strchr(sub_path, '/');
	} while (sub_path);

	ret = snprintf(path, sizeof(path), "%s%s", cgroup_mount, cgroup_path);
	if (ret < 0 || (size_t)ret >= sizeof(path))
		return -1;

	for (int i = 0 ; i < cgroup_depth ; i++) {
		/* Create a path to read from. */
		ret = snprintf(file_path, sizeof(file_path), "%s/pids.max", path);
		if (ret < 0 || (size_t)ret >= sizeof(file_path))
			return -1;

		max_pids = __read_pids_limit(file_path, cleanup_fn);
		if (max_pids >= 0)
			return max_pids;

		strncat(path, "/..", PATH_MAX);
	}

	if (max_pids < 0) {
		/* Read kernel imposed limits */
		SAFE_FILE_SCANF(cleanup_fn, PID_MAX_PATH, "%d", &max_pids);
		tst_resm(TINFO, "Using kernel processes limit of %d",
			 max_pids);
	}

	return max_pids;
}

static int get_session_pids_limit(void (*cleanup_fn) (void))
{
	char path[PATH_MAX + 1];
	char cgroup_pids[PATH_MAX + 1];
	char catchall;
	int ret = 0;

	/* Check for generic cgroup v1 pid.max */
	ret = FILE_LINES_SCANF(cleanup_fn, "/proc/self/cgroup",
						   "%*d:pids:%s\n", cgroup_pids);
	/*
	 * This is a bit of a hack of scanf format strings. Indeed, if all
	 * conversion specifications have been matched the return of scanf will be
	 * the same whether any outstanding literal characters match or not.
	 * As we want to match the literal part, we can add a catchall after it
	 * so that it won't be counted if the literal part doesn't match.
	 * This makes the macro go to the next line until the catchall, thus
	 * the literal parts, is matched.
	 *
	 * Assume that the root of the mount is '/'. It can be anything,
	 * but it should be '/' on any normal system.
	 */
	if (!ret)
		ret = FILE_LINES_SCANF(cleanup_fn, "/proc/self/mountinfo",
							   "%*s %*s %*s %*s %s %*[^-] - cgroup %*s %*[rw],pid%c",
							   path, &catchall);

	if (!ret)
		return read_session_pids_limit(path, cgroup_pids, cleanup_fn);

	/* Check for generic cgroup v2 pid.max */
	ret = FILE_LINES_SCANF(cleanup_fn, "/proc/self/cgroup",
						   "%*d::%s\n", cgroup_pids);
	if (!ret)
		ret = FILE_LINES_SCANF(cleanup_fn, "/proc/self/mountinfo",
							   "%*s %*s %*s %*s %s %*[^-] - cgroup2 %c",
							   path, &catchall);

	if (!ret)
		return read_session_pids_limit(path, cgroup_pids, cleanup_fn);

	return -1;
}

int tst_get_free_pids_(void (*cleanup_fn) (void))
{
	FILE *f;
	int rc, used_pids, max_pids, max_session_pids;

	f = popen("ps -eT | wc -l", "r");
	if (!f) {
		tst_brkm(TBROK, cleanup_fn, "Could not run 'ps' to calculate used pids");
		return -1;
	}
	rc = fscanf(f, "%i", &used_pids);
	pclose(f);

	if (rc != 1 || used_pids < 0) {
		tst_brkm(TBROK, cleanup_fn, "Could not read output of 'ps' to calculate used pids");
		return -1;
	}

	SAFE_FILE_SCANF(cleanup_fn, PID_MAX_PATH, "%d", &max_pids);

	max_session_pids = get_session_pids_limit(cleanup_fn);
	if ((max_session_pids > 0) && (max_session_pids < max_pids))
		max_pids = max_session_pids;

	if (max_pids > PIDS_RESERVE)
		max_pids -= PIDS_RESERVE;
	else
		max_pids = 0;

	/* max_pids contains the maximum PID + 1,
	 * used_pids contains used PIDs + 1,
	 * so this additional '1' is eliminated by the substraction */
	if (used_pids >= max_pids) {
		tst_brkm(TBROK, cleanup_fn, "No free pids");
		return 0;
	}
	return max_pids - used_pids;
}
