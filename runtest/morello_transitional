#DESCRIPTION: Morello transitional ABI system calls

access01 access01
access02 access02
access03 access03
access04 access04

chdir01 chdir01
chdir04 chdir04

clock_gettime01 clock_gettime01
clock_gettime02 clock_gettime02
clock_gettime03 clock_gettime03
clock_gettime04 clock_gettime04

clone01 clone01
clone02 clone02
clone03 clone03
#MUSL - depends on Musl release >= morello-release-1.5.0
clone04 clone04
clone05 clone05
clone06 clone06
clone07 clone07
clone08 clone08
clone09 clone09

clone301 clone301
clone302 clone302

close01 close01
close02 close02

execve01 execve01
execve02 execve02
execve03 execve03
execve04 execve04
execve05 execve05 -i 5 -n 32
execve06 execve06

exit01 exit01
exit02 exit02

exit_group01 exit_group01

faccessat01 faccessat01

fchdir01 fchdir01
fchdir02 fchdir02
fchdir03 fchdir03

fcntl01 fcntl01
fcntl02 fcntl02
fcntl03 fcntl03
fcntl04 fcntl04
fcntl05 fcntl05
fcntl06 fcntl06
fcntl07 fcntl07
fcntl08 fcntl08
fcntl09 fcntl09
fcntl10 fcntl10
fcntl11 fcntl11
fcntl12 fcntl12
fcntl13 fcntl13
fcntl14 fcntl14
fcntl15 fcntl15
fcntl16 fcntl16
fcntl17 fcntl17
fcntl18 fcntl18
fcntl19 fcntl19
fcntl20 fcntl20
fcntl21 fcntl21
fcntl22 fcntl22
fcntl23 fcntl23
fcntl24 fcntl24
fcntl25 fcntl25
fcntl26 fcntl26
fcntl27 fcntl27
fcntl28 fcntl28
fcntl29 fcntl29
fcntl30 fcntl30
fcntl31 fcntl31
fcntl32 fcntl32
fcntl33 fcntl33
fcntl34 fcntl34
fcntl35 fcntl35
fcntl36 fcntl36
fcntl37 fcntl37
fcntl38 fcntl38
fcntl39 fcntl39

fstat02 fstat02
fstat03 fstat03

fstatat01 fstatat01

fstatfs01 fstatfs01
fstatfs02 fstatfs02

futex_cmp_requeue01 futex_cmp_requeue01
futex_cmp_requeue02 futex_cmp_requeue02
futex_wait01 futex_wait01
futex_wait02 futex_wait02
futex_wait03 futex_wait03
futex_wait04 futex_wait04
futex_wait05 futex_wait05
futex_waitv01 futex_waitv01
futex_waitv02 futex_waitv02
futex_waitv03 futex_waitv03
futex_wake01 futex_wake01
futex_wake02 futex_wake02
futex_wake03 futex_wake03
futex_wake04 futex_wake04
futex_wait_bitset01 futex_wait_bitset01

#MUSL - depends on Musl release >= morello-release-1.5.0
getcwd01 getcwd01
getcwd02 getcwd02
getcwd03 getcwd03
getcwd04 getcwd04

getegid01 getegid01
getegid02 getegid02

geteuid01 geteuid01
geteuid02 geteuid02

getgid01 getgid01
getgid03 getgid03

gethostid01 gethostid01

getitimer01 getitimer01
getitimer02 getitimer02

getpgid01 getpgid01
getpgid02 getpgid02

getpid01 getpid01
getpid02 getpid02

getppid01 getppid01
getppid02 getppid02

getrandom01 getrandom01
getrandom02 getrandom02
getrandom03 getrandom03
getrandom04 getrandom04

getresgid01 getresgid01
getresgid02 getresgid02
getresgid03 getresgid03

getresuid01 getresuid01
getresuid02 getresuid02
getresuid03 getresuid03

getsid01 getsid01
getsid02 getsid02

gettid01 gettid01

gettimeofday01 gettimeofday01
gettimeofday02 gettimeofday02

getuid01 getuid01
getuid03 getuid03

kill02 kill02
kill03 kill03
#KERN - depends on Morello Linux kernel release >= morello-release-1.5.0
kill05 kill05
kill06 kill06
#KERN - depends on Morello Linux kernel release >= morello-release-1.5.0
kill07 kill07
kill08 kill08
kill09 kill09
kill10 kill10
kill11 kill11
kill12 kill12
kill13 kill13

lstat01 lstat01
lstat02 lstat02

madvise01 madvise01
madvise02 madvise02
madvise05 madvise05
madvise06 madvise06
madvise07 madvise07
madvise08 madvise08
madvise09 madvise09
madvise10 madvise10

mmap001 mmap001 -m 1

mmap01 mmap01
mmap02 mmap02
mmap03 mmap03
mmap04 mmap04
mmap05 mmap05
mmap06 mmap06
mmap07 mmap07
mmap08 mmap08
mmap09 mmap09
mmap10 mmap10
mmap10_1 mmap10 -a
mmap10_2 mmap10 -s
mmap10_3 mmap10 -a -s
mmap10_4 mmap10 -a -s -i 60
# test is broken/inconsistent on some setups e.g. debian soc, mask it for now.
#mmap11 mmap11 -i 30000
mmap12 mmap12
mmap13 mmap13
mmap14 mmap14
mmap15 mmap15
mmap16 mmap16
mmap17 mmap17
#MUSL - depends on Musl release >= morello-release-1.5.0
mmap18 mmap18
mmap19 mmap19

mprotect01 mprotect01
mprotect02 mprotect02
mprotect03 mprotect03
mprotect04 mprotect04

mremap01 mremap01
mremap02 mremap02
mremap03 mremap03
mremap04 mremap04
mremap05 mremap05

munmap01 munmap01
munmap02 munmap02
munmap03 munmap03

open01 open01
open02 open02
open03 open03
open04 open04
open06 open06
open07 open07
open08 open08
open09 open09
open10 open10
open11 open11
open12 open12
# The wrappers for fchmod and fchown in Musl and fchown in Bionic allow O_PATH file descriptor, so skip the test.
#open13 open13
open14 open14

openat01 openat01
openat02 openat02
openat03 openat03

openat201 openat201
openat202 openat202
openat203 openat203

prctl01 prctl01
prctl02 prctl02
prctl03 prctl03
#KERN - depends on Morello Linux kernel release >= morello-release-1.5.0
prctl04 prctl04
prctl05 prctl05
prctl06 prctl06
prctl07 prctl07
prctl08 prctl08
prctl09 prctl09

pread01 pread01
pread02 pread02

pselect01 pselect01
pselect02 pselect02
pselect03 pselect03

pwrite01 pwrite01
pwrite02 pwrite02
pwrite03 pwrite03
pwrite04 pwrite04


read01 read01
read02 read02
read03 read03
read04 read04

readlink01 readlink01
readlink03 readlink03

readlinkat01 readlinkat01
readlinkat02 readlinkat02

readv01 readv01
readv02 readv02

rt_sigaction01 rt_sigaction01
rt_sigaction02 rt_sigaction02
rt_sigaction03 rt_sigaction03

rt_sigprocmask01 rt_sigprocmask01
rt_sigprocmask02 rt_sigprocmask02

rt_sigsuspend01 rt_sigsuspend01

sched_getscheduler01 sched_getscheduler01
sched_getscheduler02 sched_getscheduler02

sched_setscheduler01 sched_setscheduler01
sched_setscheduler02 sched_setscheduler02
sched_setscheduler03 sched_setscheduler03

setegid01 setegid01
setegid02 setegid02

setfsgid01 setfsgid01
setfsgid02 setfsgid02
setfsgid03 setfsgid03

setfsuid01 setfsuid01
setfsuid02 setfsuid02
setfsuid03 setfsuid03
setfsuid04 setfsuid04

setgid01 setgid01
setgid02 setgid02
setgid03 setgid03

setpgid01 setpgid01
setpgid02 setpgid02
setpgid03 setpgid03

setregid01 setregid01
setregid02 setregid02
setregid03 setregid03
setregid04 setregid04

setresgid01 setresgid01
setresgid02 setresgid02
setresgid03 setresgid03
setresgid04 setresgid04

setresuid01 setresuid01
setresuid02 setresuid02
setresuid03 setresuid03
setresuid04 setresuid04
setresuid05 setresuid05

setreuid01 setreuid01
setreuid02 setreuid02
setreuid03 setreuid03
setreuid04 setreuid04
setreuid05 setreuid05
setreuid06 setreuid06
setreuid07 setreuid07

setsid01 setsid01

setuid01 setuid01
setuid03 setuid03
setuid04 setuid04

set_tid_address01 set_tid_address01

setitimer01 setitimer01
setitimer02 setitimer02

sigaltstack01 sigaltstack01
sigaltstack02 sigaltstack02

stat01 stat01
stat02 stat02
stat03 stat03

statfs01 statfs01
statfs02 statfs02
statfs03 statfs03

tgkill01 tgkill01
tgkill02 tgkill02
tgkill03 tgkill03

unlink05 unlink05
unlink07 unlink07
unlink08 unlink08

unlinkat01 unlinkat01

wait401 wait401
wait402 wait402
wait403 wait403

#MUSL - depends on Musl release >= morello-release-1.5.0
waitid01 waitid01
waitid02 waitid02
waitid03 waitid03
waitid04 waitid04
waitid05 waitid05
waitid06 waitid06
#MUSL - depends on Musl release >= morello-release-1.5.0
waitid07 waitid07
#MUSL - depends on Musl release >= morello-release-1.5.0
waitid08 waitid08
waitid09 waitid09
#MUSL - depends on Musl release >= morello-release-1.5.0
waitid10 waitid10
#MUSL - depends on Musl release >= morello-release-1.5.0
waitid11 waitid11

write01 write01
write02 write02
write03 write03
write04 write04
write05 write05
write06 write06

writev01 writev01
writev02 writev02
writev03 writev03
writev05 writev05
writev06 writev06
writev07 writev07
