// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (c) Arm Ltd. 2022. All rights reserved.
 * Author: Kristina Martsenko <kristina.martsenko@arm.com>
 */

/*\
 * [Description]
 *
 * Check that passing a pointer as epoll_data_t succeeds.
 */

#ifdef __CHERI_PURE_CAPABILITY__
#include <cheriintrin.h>
#endif
#include <sys/epoll.h>

#include "tst_test.h"

static int epfd, fds[2];
static struct epoll_event epevs[1] = {
	{.events = EPOLLOUT}
};
static int test_data;

static void run(void)
{
	struct epoll_event ret_evs = {.events = 0, .data.ptr = NULL};

	TEST(epoll_wait(epfd, &ret_evs, 1, -1));

	if (TST_RET == -1) {
		tst_res(TFAIL | TTERRNO, "epoll_wait() failed");
		return;
	}

	if (TST_RET != 1) {
		tst_res(TFAIL, "epoll_wait() returned %li, expected 1", TST_RET);
		return;
	}

#ifdef __CHERI_PURE_CAPABILITY__
	if (!cheri_is_equal_exact(ret_evs.data.ptr, &test_data)) {
		tst_res(TFAIL, "epoll_wait() received different pointer");
		return;
	}
#else
	if (ret_evs.data.ptr != &test_data) {
		tst_res(TFAIL, "epoll_wait() received different pointer");
		return;
	}
#endif

	tst_res(TPASS, "epoll_wait() with pointer as epoll_data_t");
}

static void setup(void)
{
	SAFE_PIPE(fds);

	epfd = epoll_create(1);
	if (epfd == -1)
		tst_brk(TBROK | TERRNO, "epoll_create()");

	epevs[0].data.ptr = &test_data;

	if (epoll_ctl(epfd, EPOLL_CTL_ADD, fds[1], &epevs[0]))
		tst_brk(TBROK | TERRNO, "epoll_ctl(..., EPOLL_CTL_ADD, ...)");
}

static void cleanup(void)
{
	if (epfd > 0)
		SAFE_CLOSE(epfd);

	if (fds[0]) {
		SAFE_CLOSE(fds[0]);
		SAFE_CLOSE(fds[1]);
	}
}

static struct tst_test test = {
	.test_all = run,
	.setup = setup,
	.cleanup = cleanup,
};
